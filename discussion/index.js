console.log('Hello world!')

//[Section] Control Structures

//[Subsection] If-else statement

let numA = 3; //assessing the value

//If statement/branch
//The task of the if statement is to execute a procedure/action if the specified condition is "true".

if (numA <= 0) {
	//truthy branch
	//this block of code will run if the condition is met.
	console.log('The condition was met!');
}

// let name = 'Lorenzo';
//Maria - not allowed

let isLegal = true;


//create a control structure that will allow the user to proceed if the value matches/passes the condition.
if (isLegal) {
	//if the passes the condition the truthy branch will run.
	console.log('User can proceed!')
}

//[Subsection] Else Statement

//This executes a statement if ALL other conditions are false and/or has failed.

//Create a control structure that will allow us to simulate a user login.

//Prompt Box - prompt(): this will allow us to display a prompt dialog box which we can the user for an input.
//Syntax: prompt(text/message [required], placeholer/default text)[optional];
// prompt("Please enter your first name:", "Martin");

//Create a control structure that will allow us to check of the user is old enough to drink.

// let age = 21;

//alert() - display a message box to the user which would require their attention
// if (age >= 18) {
// 	alert("Cheers mate!");
// } else {
// 	//falsy branch
// 	alert("Not today, son!");
// }


//Create a control structure that will ask for the number of drinks that you'll order.

//Ask the user how many drinks he wants.
// let order = prompt('How many drinks do you like?');

//convert the string data type to number data type.
//parseInt() - will allow us to convert strings to integers.
// console.log(order);
// order = parseInt(order);
//Create a logic that will make sure that the user's input is greater than 0.

//Type coercion - conversion data type was converted to another data type.
//1. If one of the operands in an object, it will be converted into a primitive data type. (string, number, boolean.)
//2. If at least one operand is a string, it will convert the other operand into a string as well.
//3. If both numbers are numbers then an arithmetic operation will be executed.

//In JS there are 3 ways to multiply a string.
	//1. repeat() method - this will allow us to return a new string value that contains the number of copies of the string. (string.repeat(value/number))
	//2. loops - for loop
	//3. loops - while loop method
// if (order > 0) {
// 	console.log(typeof order);
// 	alert("🍸".repeat(order));
// } else {
// 	alert('The number should be above 0');
// }

// if (order > 0) {
// 	console.log(typeof order);
// 	let textAlert = 'Number of drinks: ';
// 	const drinkText = '🍸';

// 	let i = 0;
	//Loop condition
// 	while(i < order) {
// 		textAlert += drinkText;
// 		i = i + 1;
// 	}
// 	alert (textAlert);
// }

// Mini-Task: Vaccine Checker
function vaccineChecker(){
	// Ask information from the user
	let vax = prompt('What brand is your vaccine?')
	console.log(vax);
	//Process the input of the user so that whatever input she will enter, we can control the uniformity of the character casing
	 //toLowerCase() - will allow us to convert a string into all lowercase characters. (Syntax: string.toLowerCase() ) 
	 vax = vax.toLowerCase();
	 // Create a logic that will allow us to check the values inserted by the user to match the given parameters.
	 if (vax === 'pfizer' || vax === 'moderna' || vax === 'astrazeneca' || vax === 'janssen') {
	 	// Display the response back to the client.
	 	alert(vax + ' is allowed to travel.');
	 } else {
	 	alert('sorry not permitted');
	 }
}

//We want the user to be able to invoke this function with the user of a trigger.
// vaccineChecker();

// onclick - is an example of a JS event. This 'event' in JS occurs when the user

//Nested if-else (syntax)
function determineTyphoonIntensity() {
	//Going to need an input from the user.
	// We need a 'number' data type so that we can properly compare the values.
	let windspeed =	parseInt(prompt('Wind speed:'));
	console.log(typeof windspeed); //this is to prove that we can directly pass the value of the variable and feed to the parseInt method.
	//Note: else if is 2 words.
	if (windspeed < 30) {
		alert('Not a typhoon.');
	} else if (windspeed <= 61) {
		// This will run if the 2nd statement was met.
		alert('Tropical Depression Level 1');
	} else if (windspeed <= 88) {
		alert('Tropical Depression Level 2');
	} else if (windspeed <= 117) {
		alert('Tropical Depression Level 3');
	}
	else {
		alert('Typhoon Detected');
	}
}

// determineTyphoonIntensity();

//Conditional Ternary

//It still follows the same syntax with an if-else
//(Truthy, Falsy)
//This is the only JS Operator that takes 3 operands

// ? Question Mark - This would describe a condition that if resulted to "true" will run "truthy" (Syntax: condition ? "truthy" : "falsy")
// : Colon - This would separate the truthy and falsy statements.
function ageChecker() {
	let age = parseInt(prompt('How old are you?'))
	

	//Simplified structure using Ternary operator
	return (age >= 18) ? alert('Can vote') : alert('No can do');

}
// 	if(age >= 18) {
// 		alert('Can vote');
// } else {
// 	alert('No can do');
// 	}

// Create a function that will determine the owner of the computer.
function determineComputerOwner() {
	let unit = prompt('What is the unit number?');

	let bts;
	//The unit - represents the unit number.
	//The manggagamit - represent the user who owns the unit.
	switch (unit) {
		//Declare multiple cases to represent each outcome that will match the value inside the expression
		case '1':
			bts = "Min Yoongi";
			break;
		case '2':
			bts = "Park Jimin";
			break;
		case '3':
			bts = "Kim Namjoon";
			break;
		case '4':
			bts = "Jung Hoseok";
			break;
		case '5':
			bts = "Jeon Jungkook";
			break;
		case '6':
			bts = "Kim Seokjin";
			break;
		case '7':
			bts = "Kim Taehyung";
			break;
		// If all else fails or if non of the preceeding cases above meets the expression, this statement will become the fail safe/default response.
		default:
			bts = 'NAH UH GIRL';
	}
	console.log(bts);
}

//When to use "" (double quote) and '' (single quote)

// "name" === 'name'
//We can use either methods to declare a string value. However, we can use one over the other to escape the scope of initial symbol.

let dialog = '"Drink your water bhie" mimiyuuh says.'
console.log(dialog);