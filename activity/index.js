console.log('Hello World')

function doEquation() {
	let x = prompt('Provide a number')
	let y = prompt('Provide another number')

	let sum = parseInt(x) + parseInt(y)

	if(sum < 10) {
		alert('The sum of the two numbers is: ' + sum);
	} else if (sum <= 20) {
		alert('The difference of the two numbers is: ' + (parseInt(x) - parseInt(y)));
	} else if (sum <= 29) {
		alert('The product of the two numbers is: ' + (parseInt(x) * parseInt(y)));
	} else if(sum >= 30) {
		alert('The quotient of the two numbers is: ' + (parseInt(x) / parseInt(y)));
	}
}

function getNameAndAge() {
	let name = prompt('What is your name?')
	console.log(name);
	let age = prompt('Your age?')

	if(name === '' || age === '') {
		alert('Are you a time traveler?')
	} else if(name !== '' && age !== '') {
		alert("The user's name is - " + name + " and user's age is - " + age)
	}
}

function getUserAge() {
	let age = prompt('What is your age?')

	let partyGoer;

	switch(age) {
		case '18':
			partyGoer = 'You are now allowed to party.'
			break;
		case '21':
			partyGoer = 'You are now part of the adult society.'
			break;
		case '65':
			partyGoer = 'We thank you for your contribution to the society.'
			break;
		default:
			partyGoer = "Are you sure you're not an alien?"
	}
	alert(partyGoer);
}